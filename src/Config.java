/**
 * Created by Janus on 2018/09/26.
 */
public class Config {
    int oscBroadcastPort = 30000;
    int oscListeningPort = 33000;
    String oscBroadcastIp="127.0.0.1";
    public int serialBaudRate=  9600;;
    public String serialPortName="COM7";
}
