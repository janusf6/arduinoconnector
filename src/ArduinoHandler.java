import processing.core.PApplet;
import processing.serial.Serial;
import java.util.ArrayList;

/**
 * Created by Janus on 2018/09/26.
 */
public class ArduinoHandler {



    private   int endLine=10;
    public Serial serialPort;
    private ArrayList<Byte> byteBuffer;
    private Config config;
    private PApplet p;

    public ArduinoHandler(Config config, PApplet p) {

        this.config = config;
        this.p = p;
    }

    public void establishConnection() {
        try {
            serialPort = new Serial(p, config.serialPortName, config.serialBaudRate);
             serialPort.buffer(40);
            //   serialPort.buffer(10);

            if(serialPort.active())
                System.out.println(" Arduino connection established: "+config.serialPortName);
            while(serialPort.available()>0)
            {
                serialPort.clear();
            }

        }catch (RuntimeException e){
            System.out.println(" Arduino not connected:" +e.getMessage());
        }
    }



}
