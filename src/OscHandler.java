import netP5.NetAddress;
import oscP5.OscMessage;
import oscP5.OscP5;

/**
 * Created by Janus on 2018/09/26.
 */

public class OscHandler {
    OscP5 oscP5;
    NetAddress address ;

    public OscHandler(Config config) {
        oscP5 = new OscP5(this, config.oscListeningPort);
        address= new NetAddress(config.oscBroadcastIp ,config.oscBroadcastPort);
    }

    public void  sendSwitchMessage(int key,int val)
    {
        OscMessage message = new OscMessage("/switch");
        message.add(key);
        message.add(val);
        oscP5.send(message, address);

    }
    public void  sendTriggerMessage(int key)
    {
        OscMessage message = new OscMessage("/trigger");
        message.add(key);
        message.add(1);
        oscP5.send(message, address);
    }
    public void  sendVaryingMessage(int key,float val)
    {
        OscMessage message = new OscMessage("/varying");
        message.add(key);
     message.add(val);
      oscP5.send(message, address);
//        System.out.println ( "varying! ");
    }
}
