import processing.core.PApplet;
import processing.core.PGraphics;
import processing.serial.Serial;

/**
 * Created by Janus on 2018/01/03.
 */
public class Main extends PApplet {
    int h = 300;
    int w = 400;
    int key;
    float val;
//    private PGraphics buffer;
Config config;
    ArduinoHandler arduinoHandler;
    OscHandler oscHandler;




    private String startTime;



    public static void main(String args[]) {
        PApplet.main(new String[]{Main.class.getName()});
    }

    public void settings() {
        size(w   , h   , P2D);
    }

    public void setup() {
        config=new Config();
        arduinoHandler=new ArduinoHandler(config, this);
        oscHandler=new OscHandler(config);

        arduinoHandler.establishConnection();


//        startTime = year() + nf(month(), 2) + nf(day(), 2) + "-" + nf(hour(), 2) + nf(minute(), 2) + nf(second(), 2);
//        initBuffer();
        frameRate(60);

    }

//    private void initBuffer() {
//        buffer = createGraphics(w, h, P2D);
//        buffer.beginDraw();
//        buffer.background(0xffffffff);
//    }

    public void draw() {
        surface.setTitle(" f" + frameCount + " | " + frameRate + "fps");
//        oscHandler.sendTriggerMessage(frameCount%4);

    }
    public  void serialEvent(Serial p) {
        if (p.available() > 0)
        {

            String inString = p.readString();
          //  System.out.println(inString);
            try {
                String[] lines=inString.split("\\r?\\n");
                for (int i = 0; i < lines.length; i++) {
                    parseMessage(lines[i]);
                }

            }
            catch (Exception e)
            {
          printStackTrace(e);
            }
        }

    }
    private void parseSerialCommand(String inString, int separatorIndex) {

        try{


            if(inString.length()>separatorIndex +3 )
            {
                String fString=inString.substring(separatorIndex +2,inString.length()-1);
                      key= Integer.parseInt(inString.substring(0, separatorIndex));
                val= Float.parseFloat(fString);
                val=round(val*100)/100;

            }

        }catch (Exception e)
        {

//            printStackTrace(e);
//            println( );
           // println( "' key: '"+key + "' value: '"+val+"'");
        }

    }
    private void parseMessage(String inString) {
//        System.out.println(inString);
         ;
        int keyI=0;

         if(inString.startsWith("v"))
             keyI=1;

         if(inString.startsWith("s"))
             keyI=2;


         if(inString.startsWith("t"))
             keyI=3;


//       println (inString+ "k:"+keyI +" ["+inString.substring(1,2)+"]");
        if(inString.length()>2&& keyI>0 &&inString.substring(1,2).matches("[0-9]"))
        {
//
            inString=inString.substring(1);
            int separatorIndex = inString.indexOf(':');
//            println (inString+"//"+separatorIndex);
            if (separatorIndex>0 )
            {
                parseSerialCommand(inString, separatorIndex);

                if(key>-1)
                {

                    switch (keyI){
                        case 1:
                            oscHandler.sendVaryingMessage(key,val);
                            break;
                        case 2:
                            oscHandler.sendSwitchMessage(key,(int)val);
                            break;
                        case 3:
                            oscHandler.sendTriggerMessage(key );
                            break;
                        default:
                            break;
                    }


                }
            }

        }
    }


    private void update() {

    }
}
