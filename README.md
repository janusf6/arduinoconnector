# ArduinoConnector

small processing project to send osc messages based on arduino sensor input. Pairs with arduinoSensors project.
osc  messages are: 
S i [0..1] -switches
V i [0..255] -variable
T i [1] -triggers